<?php
script('podcast', 'script');
style('podcast', 'style');
?>

<div id="app" ng-app="podcast" ng-cloak ng-init="started = false; lang = '<?php p($_['lang']) ?>'">

		<div id="app-navigation">
			<?php print_unescaped($this->inc('navigation/index')); ?>
			<?php print_unescaped($this->inc('settings/index')); ?>
		</div>
		<div id="app-content">
			<div id="app-content-wrapper">
				<?php print_unescaped($this->inc('content/index')); ?>
			</div>
		</div>

</div>
