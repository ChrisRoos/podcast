<?php

namespace OCA\Podcast\Controller;

use \OCP\AppFramework\Controller;
use \OCP\AppFramework\Http\TemplateResponse;
use \OCP\IRequest;
use OCP\Files\Folder;
use OCA\Files\Helper;
use OCP\Files\Search\ISearchQuery;
use OCP\Files\Mount\IMountPoint;
use OC\Files\Filesystem;
use OC\Files\Storage\DAV;
use \OC\Files\Utils\Scanner;

class UploadController extends Controller{
    public $extensao;
    public $novo_nome;
    public $dir;
    public $path;
    public $nome_ep;
    public $descri;

  public function scanFolder($path)
    {
		$response = array();
        $user = \OC::$server->getUserSession()->getUser()->getUID();
		$scanner = new Scanner ($user, \OC::$server->getDatabaseConnection(), \OC::$server->getLogger());
		try {
            $scanner->scan($path, $recusive = false);
        } catch (ForbiddenException $e) {
			$response = array_merge($response, array("code" => 0, "desc" => $e));
			return json_encode($response);
        }catch (NotFoundException $e){
			$response = array_merge($response, array("code" => 0, "desc" => "Can't scan file at ".$path));
			return json_encode($response);
		}catch (\Exception $e){
			$response = array_merge($response, array("code" => 0, "desc" => $e));
			return json_encode($response);
		}
		return 1;
	} 




  public function __construct($AppName, IRequest $request){
    parent::__construct($AppName, $request);
    }
    /**
       * @NoAdminRequired
    * @NoCSRFRequired
    */
    
     public  function getUploadFile($myfile) {
              $file = $this->request->getUploadedFile($myfile);  // $_FILES['myfile']
              return $file ;
         }

     public  function getParamFile($myfile) {
              $file = $this->request->getParam($myfile);  // $_FILES['myfile']
              return $file ;
         }

      static public function fopen($path, $mode) {
		     return self::$defaultInstance->fopen($path, $mode);
	}

      static public function file_put_contents($path, $data) {
          return self::$defaultInstance->file_put_contents($path, $data);
        }
     /**
    * @NoAdminRequired
    * @NoCSRFRequired
     */
    public function index() {
      return new TemplateResponse('podcast', 'index');
    }

    /**
    * @NoAdminRequired
    * @NoCSRFRequired
    */

    public function enviar() {

      $file = $this->getUploadFile('fileUpload');
      $folder_cast = "Podcast/".$this->getParamFile("nome");
      $userRaiz = Filesystem::getRoot(); // pega a raiz do user
      $descri = $_REQUEST['descri'];

      if(isset($file)){
          Filesystem::mkdir($folder_cast); //deve criar uma pasta com o nome do EP dentro da /Podcast

          for ($number = 0; $number <= 1; $number++) {
                  if (!is_dir($userRaiz)){
                    $fileName =$file['name'][$number];
                    $fileTmpName =$file['tmp_name'][$number];
                    $fileSize =$file['size'][$number];
                    $fileError =$file['error'][$number];
                    $fileType =$file['type'][$number]; //extensao do arquivo
                    $fileExt = explode('/',$fileType); //Separa para encontrar a ext
                    $fileActualExt = strtolower(end($fileExt)); //renomeia para minusculo

                    //cria o arquivo descri
                    Filesystem::fopen($folder_cast."/"."Descrição.txt","a+");
                    //Adiciona o conteudo ao arquivo criado
                    Filesystem::file_put_contents($folder_cast."/"."Descrição.txt",$descri);
                    //verficar se o caminho existe
                    Filesystem::fromTmpFile($fileTmpName,$folder_cast."/".$fileName);
                                   }
            }
        return new TemplateResponse('podcast', 'index'); // Retorna para o template original
        // }
        // }
    }
}
}

?>
